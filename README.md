# deSIC Network - _API (Application Programming Interface)_

### Description
A Data management tool for third sector organisations.

For updates in the project please visit our website [desic.io](https://www.desic.io).

This project was realised with funds from the kind people over at [NotEqual Network](https://not-equal.tech). 

>API is part 1 out of 2. (You can find the other part [here](https://gitlab.com/sociality/desic-app.git))

### Git
Clone [deSIC API](https://gitlab.com/sociality/desic-api.git):
`git clone https://gitlab.com/sociality/desic-api.git`
Navigate to it `cd desic-api` & run `npm install` to install dependencies.

### Set up DB (PostgreSQL)
First, you need to set up a new PostgreSQL Database ([local](https://www.prisma.io/dataguide/postgresql/setting-up-a-local-postgresql-database) or [remote](https://www.heroku.com/postgres)).
Afterwards, you will be asked to provide these credential into the file that you will create in the next step.

### Run Locally
Create a **.env** file into the root folder. 
```
PRODUCTION=[true or false. If it is false, every email will be sent to the EMAIL_TEST]

ADMIN_URL=[Admin Application's URL ex. https://my-admin.domain.com]
APP_URL=[Client Application's URL ex. https://my-app.domain.com]

API_URL=[API's URL ex. https://my-api.domain.com]
PORT=[API's URL ex. 3000]

POSTGRES_HOST=[DB's Host ex. localhost or 192.192.192.192]
POSTGRES_PORT=[DB's Port ex. 5432]
POSTGRES_USER=[DB's User ex. root]
POSTGRES_PASSWORD=[DB's Password ex. *********]
POSTGRES_DB=[DB's Name ex. localhost or 192.192.192.192]

EMAIL_HOST=[Email Service's Host ex. gmail]
EMAIL_PORT=[Email Service's Port ex. 587]
EMAIL_SERVICE=[Email Service's Name ex. gmail]
EMAIL_USER=[Email Service's Name ex. my-email@domain.com]
EMAIL_PASSWORD=[Email Service's Name ex. ********]
EMAIL_FROM=[Email Service's Name ex. My App <my-email@domain.com>]
EMAIL_TO=['Contact to Us' Email Address ex. my-email@domain.com]
EMAIL_TEST=[An email address to which, every email will be sent when PRODUCTION is false]

TOKEN_LENGTH=32
TOKEN_EXPIRATION=3600

JWT_SECRET=[An alphanumerical for JWT ex. aSecretGoesHere]
JWT_EXPIRATION=36000

CRYPTO_ALGORITHM=aes-256-ctr
CRYPTO_SECRET_KEY=[An alphnumerical for Crypto ex. anotherSercretGoesHere]
```

Run `npm run start` to execute the API (starts on [localhost:3000](http://localhost:3000)).

### Build API
Run `npm run build` to build the project. The build artifacts will be stored 
in the `dist` directory. 
Serve via `dist/App.js` file.
Or run using `npm run start` and access them via [localhost](http://localhost:3000/)