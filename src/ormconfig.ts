import { ConnectionOptions } from 'typeorm';

import {
  OrganizationEntity, UserEntity, UserProfileEntity, UserAuthEntity, RefreshTokenEntity,
  ConnectionEntity, IncidentEntity, SessionEntity, ExaminationEntity, PostEntity, CategoryEntity, AccessHistoryEntity
} from './entities/index';

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  entities: [

    UserEntity,
    UserAuthEntity,
    UserProfileEntity,
    RefreshTokenEntity,
    AccessHistoryEntity,

    ConnectionEntity,
    OrganizationEntity,

    IncidentEntity,
    SessionEntity,
    ExaminationEntity,

    CategoryEntity,
    PostEntity
  ],
  cli: {
    migrationsDir: 'src/migrations',
  },
  ssl: { rejectUnauthorized: false },
  synchronize: true
};

export default config;
