import { NextFunction, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';

/**
 * Entities
 */
import { UserEntity } from '../entities/lib/user.entity';

/**
 * Interfaces
 */
import { RequestWithUser } from '../interfaces/lib/requestWithUser.interface';
import { EncryptedUserInterface } from '@interfaces';

async function authMiddleware(request: RequestWithUser, response: Response, next: NextFunction) {

  const userRepository = getRepository(UserEntity);

  const token = request.headers;
  if (token && token.authorization) {
    const secret = process.env.JWT_SECRET;
    try {
      const verificationResponse = jwt.verify((token.authorization).replace('Bearer ', ''), secret) as { _id: number };
      const id = verificationResponse._id;
      const user: EncryptedUserInterface = await userRepository.findOne(id);
      // const user: EncryptedUserInterface = await userRepository.findOne('9119bc58-d3ff-4355-8e4d-967ec524ba37')
      if (user) {
        request.user = user;
        next();
      } else {
        return response.status(401).send({
          message: 'No Authenticated User',
          success: false
        });
      }
    } catch (error) {
      return response.status(401).send({
        message: 'No Authenticated User',
        success: false
      });
    }
  } else {
    return response.status(401).send({
      message: 'No Authenticated User',
      success: false
    });
  }
}

export default authMiddleware;
