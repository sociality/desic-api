import { NextFunction, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import to from 'await-to-ts';

/**
  * Entities
  */
import { AccessHistoryEntity } from '../entities/index';

/**
 * Exceptions
 */
import Forbidden from '../exceptions/Forbidden.exception';
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { RequestWithUser } from '../interfaces/lib/requestWithUser.interface';
import { UserRole } from '../interfaces/index';

async function adminsCannotAccess(request: RequestWithUser, response: Response, next: NextFunction) {
  const beneficiaryId: string = request.user.id;

  const accessHistoryRepositoy = getRepository(AccessHistoryEntity);

  let error: Error, clients: any[];
  [error, clients] = await to(accessHistoryRepositoy.find({ select: ["benefactorId"], where: { beneficiaryId: beneficiaryId } }))
  if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

  response.locals['access'] = clients.map((o: any) => { return o.benefactorId });
  next();

}

async function clientsCannotAccess(request: RequestWithUser, response: Response, next: NextFunction) {
  const benefactorId: string = request.user.id;

  const accessHistoryRepositoy = getRepository(AccessHistoryEntity);

  let error: Error, clients: any[];
  [error, clients] = await to(accessHistoryRepositoy.find({ select: ["beneficiaryId"], where: { benefactorId: benefactorId } }))
  if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

  response.locals['access'] = clients.map((o: any) => { return o.beneficiaryId });
  next();
}

async function isAdmin(request: RequestWithUser, response: Response, next: NextFunction) {

  if (request.user.access === UserRole.SUPERADMIN) {
    return next();
  }

  return response.status(403).send({
    message: 'No Access here',
    success: false
  });
}

async function fetchUsersByAccess(request: RequestWithUser, response: Response, next: NextFunction) {
  if (request.user.access < parseInt(request.params.access)) {
    return next(new Forbidden(`No Access Here`));
  }

  next();
}

export default { fetchUsersByAccess: fetchUsersByAccess, clientsCannotAccess: clientsCannotAccess, adminsCannotAccess: adminsCannotAccess, isAdmin: isAdmin };
