import crypto from 'crypto';

const iv = crypto.randomBytes(16);

/** 
 * Interfaces 
 */
import {EncryptInterface} from '@interfaces';

const encrypt = (text: string): EncryptInterface => {

    const cipher = crypto.createCipheriv(`${process.env.CRYPTO_ALGORITHM}`, `${process.env.CRYPTO_SECRET_KEY}`, iv);

    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    };
};

const decrypt = (hash: EncryptInterface): string => {

    const decipher = crypto.createDecipheriv(`${process.env.CRYPTO_ALGORITHM}`, `${process.env.CRYPTO_SECRET_KEY}`, Buffer.from(hash.iv, 'hex'));

    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);

    return decrpyted.toString();
};

export default {
    encrypt,
    decrypt
};