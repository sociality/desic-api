import {
    cleanEnv, str, port, num
} from 'envalid';

export default function validateEnv() {
    cleanEnv(process.env, {
        APP_URL: str(),
        API_URL: str(),
        PORT: port(),

         POSTGRES_HOST: str(),
         POSTGRES_PORT: port(),
         POSTGRES_USER: str(),
         POSTGRES_PASSWORD: str(),
         POSTGRES_DB: str(),

        EMAIL_HOST: str(),
        EMAIL_PORT: port(),
        EMAIL_USER: str(),
        EMAIL_PASSWORD: str(),
        EMAIL_SERVICE: str(),

        EMAIL_FROM: str(),
        EMAIL_TO: str(),
        
        TOKEN_LENGTH: num(),
        TOKEN_EXPIRATION: num(),

        JWT_SECRET: str(),
        JWT_EXPIRATION: num(),

        CRYPTO_ALGORITHM: str(),
        CRYPTO_SECRET_KEY: str()
    });
}