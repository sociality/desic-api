import { Column, Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { AccessAction, AccessCategory, AccessEditor, AccessStatus } from '../../interfaces/index';

@Entity()
export class AccessHistoryEntity {
    @PrimaryGeneratedColumn()
    public id?: number;

    @Column()
    public action: AccessAction; // read write

    @Column()
    public category: AccessCategory; // law, medical, social

    @Column()
    public editor: AccessEditor; /* user, superadmin */

    @Column()
    public status: AccessStatus; // revoke, neutral, invoke

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    beneficiary: UserEntity;

    @Column()
    beneficiaryId: string;

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    benefactor: UserEntity;

    @Column()
    benefactorId: string;

    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
    public createdAt: Date;

    @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public updatedAt: Date;
}
