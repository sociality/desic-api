import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, CreateDateColumn, UpdateDateColumn, JoinColumn } from "typeorm";
// import { UserEntity } from './user.entity';
 import { UserEntity } from './user.entity';

@Entity()
export class RefreshTokenEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

     @ManyToOne(type => UserEntity)
     @JoinColumn()
     user: UserEntity;

    @Column()
    jwtId: string;

    @Column({ default: false })
    used: boolean;

    @Column({ default: false })
    invalidated: boolean;

    @Column()
    expiresAt: Date;

    // meta data information
    @CreateDateColumn()
    creationDate: Date;

    @UpdateDateColumn()
    updateDate: Date;
}
