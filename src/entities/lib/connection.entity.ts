import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { EncryptInterface } from '@interfaces';

@Entity()
export class ConnectionEntity {
  @PrimaryGeneratedColumn()
  public id?: number;

  @ManyToOne(type => UserEntity)
  @JoinColumn()
  user_a: UserEntity;

  @Column()
  userAId: string;

  @ManyToOne(type => UserEntity)
  @JoinColumn()
  user_b: UserEntity;

  @Column()
  userBId: string;

  @Column()
  secret: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  public createdAt: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  public updatedAt: Date;
}
