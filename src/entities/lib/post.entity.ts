import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import {PostType} from '../../interfaces/index';

@Entity()
export class PostEntity {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public slug: string;

  @Column()
  public title: string;

  @Column()
  public description: string;

  @Column({ nullable: true })
  public image_url: string;

  @Column({ nullable: true })
  public when?: Date;

  @Column({ nullable: true })
  public where?: string;

  @Column()
  public type: PostType; // event, post

  @Column({ nullable: true })
  public contentFiles: string;

  @Column('timestamp with time zone', { nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;
}

export default PostEntity;
