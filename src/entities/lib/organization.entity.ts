import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { AddressInterface } from '@interfaces';

@Entity()
export class OrganizationEntity {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public name: string;

  @Column({ nullable: true })
  public image_url: string;

  @Column({ nullable: true })
  public email: string;

  @Column()
  public phone: string;

  @Column({ nullable: true })
  public phone_2: string;

  @Column({ nullable: true })
  public fax: string;

  @Column({ type: 'json' })
  public address: AddressInterface;

  @Column({ nullable: true })
  public description: string;

  @Column({ nullable: true })
  public description_2: string;

  @Column()
  public social: string;
}
