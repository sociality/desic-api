import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';

import { EncryptInterface } from '@interfaces';
// import { UserEntity } from './user.entity';
import { UserEntity } from './user.entity';

@Entity()
export class UserAuthEntity {
  @PrimaryGeneratedColumn()
  public id?: string;

  @Column({ nullable: true })
  public verificationToken?: string;

  @Column({ nullable: true })
  public verificationExpiresIn?: string;

  @Column({ default: true })
  public emailVerified: boolean;

  @Column({ nullable: true })
  public restorationToken?: string;

  @Column({ nullable: true })
  public restorationExpiresIn?: string;

  @Column({ default: true })
  public passwordVerified: boolean;

  @OneToOne(type => UserEntity)
  @JoinColumn()
  user: UserEntity;

  @Column()
  userId: string;
}
