import { IsString, IsBoolean } from 'class-validator';

export class SessionDto {
  @IsString()
  public title: string;

  @IsString()
  public description: string;

  @IsString()
  public beneficiaryId: string;

  @IsString()
  public sessionDate: Date;

  @IsString()
  public keywords: string;

  @IsString()
  public category: string;
}