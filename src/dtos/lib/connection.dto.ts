import { IsString } from 'class-validator';

export class ConnectionDto {
  @IsString()
  public connectionId: string;
}
