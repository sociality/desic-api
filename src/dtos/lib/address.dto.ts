import { IsString, ValidateNested } from 'class-validator';

export class AddressDto {
  @IsString()
  public steet: string;

  @IsString()
  public postcode: string;

  @IsString()
  public city: string;
}
