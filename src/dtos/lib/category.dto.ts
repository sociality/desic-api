import { IsString, IsNumber, IsOptional, IsArray } from 'class-validator';
import { CategoryInterface } from '../../interfaces/index';

export class CategoryDto {
  @IsArray()
  categories: CategoryInterface[];

  // @IsNumber()
  // @IsOptional()
  // public id: number;

  // @IsString()
  // public name: string;
}
