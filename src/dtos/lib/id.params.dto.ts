import { IsString } from 'class-validator';

export class ID {
    @IsString()
    public id: string;
}