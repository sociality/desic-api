import { IsString, IsEmail } from 'class-validator';

export class UpdationsDto {
    @IsString()
    public password: string;

    @IsString()
    public updated: string;
}