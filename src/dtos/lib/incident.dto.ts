import { IsString, IsBoolean } from 'class-validator';

export class IncidentDto {
    @IsString()
    public title: string;

    @IsString()
    public description: string;

    @IsString()
    public beneficiaryId: string;

    @IsString()
    public incidentDate: Date;

    @IsString()
    public keywords: string;

    @IsString()
    public category: string;
}