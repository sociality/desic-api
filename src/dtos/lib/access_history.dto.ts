import { AccessHistoryInterface } from '../../interfaces/index';
import { IsString, IsNumber, IsArray } from 'class-validator';

export class AccessHistoryDto {
  @IsArray()
  accesses: AccessHistoryInterface[];
  // @IsNumber()
  // public accessId: number;

  // @IsString()
  // public accessStatus: string;

  // @IsString()
  // public benefactorId: string;
}
