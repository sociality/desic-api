import { IsString, IsArray, IsOptional, ValidateNested } from 'class-validator';

export class PostDto {
  @IsString()
  public title: string;

  @IsString()
  public description: string;

  @IsOptional()
  @IsString()
  public image_url: string;

  @IsOptional()
  @IsString()
  public when: Date;

  @IsOptional()
  @IsString()
  public where: string;

  @IsString()
  public type: string;

  @IsArray()
  public contentFiles: string[];
}
