import { UserRole } from '@interfaces';
import { IsString, IsNumber, IsEmail, IsOptional, ValidateNested } from 'class-validator';
import { ProfileDto } from './profile.dto';

export class RegistrationDto {
  @IsEmail()
  public email: string;

  @IsString()
  public name: string;

  @IsNumber()
  public access: UserRole;

  @IsOptional()
  @IsNumber()
  public category: number;

  @IsOptional()
  @ValidateNested()
  public profile: ProfileDto;
}
