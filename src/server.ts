import 'dotenv/config';
import 'reflect-metadata';
import { createConnection, ConnectionOptions } from 'typeorm';

import App from './app';
import validateEnv from './utils/validateEnv';
import config from './ormconfig';

validateEnv();

/**
 * Controllers
 */
import TestController from './controllers/test.controller';
import AuthenticationController from './controllers/authentication.controller';
import OrganizationController from './controllers/organization.controller';
import UserController from './controllers/user.controller';
import IncidentController from './controllers/incident.controller';
import SessionController from './controllers/session.controller';
import ExaminationController from './controllers/examination.controller';
import ConnectionController from './controllers/connection.controller';
import CategoryController from './controllers/category.controller';
import PostController from './controllers/post.controller';
import AccessController from './controllers/access.controller';
import StatisticsController from './controllers/statistics.controller';
import CommunicationController from './controllers/communication.controller';

(async () => {
  try {
    const connection = await createConnection(config);
    await connection.runMigrations();
  } catch (error) {
    console.log('Error while connecting to the database', error);
    return error;
  }

  const app = new App(
    [
      new TestController(),
      new OrganizationController(),

      new UserController(),
      new AuthenticationController(),
      new ConnectionController(),
      new AccessController(),

      new IncidentController(),
      new SessionController(),
      new ExaminationController(),

      new StatisticsController(),

      new CategoryController(),

      new PostController(),
      new CommunicationController()
    ],
  );
  app.listen();
})();
