import { ExaminationInterface, EncryptedUserInterface, EncryptInterface } from "@interfaces";

export interface EncryptedExaminationInterface extends ExaminationInterface {
  title: EncryptInterface;
  description: EncryptInterface;

  beneficiary: EncryptedUserInterface;
  benefactor: EncryptedUserInterface;
}
