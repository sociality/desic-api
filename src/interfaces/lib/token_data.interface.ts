export interface TokenDataInterface {
    token: string;
    expiresIn: number;
}