import { ProfileInterface } from './profile.interface';
import { AddressInterface } from './address.interface';
import { DecryptedUserInterface } from './user_decrypted.interface';

export interface DecryptedProfileInterface extends ProfileInterface {
  address: AddressInterface;
  phone: string;
  dateOfBirth: Date;
  maritalStatus: string;
  employmentStatus: string;

  user?: DecryptedUserInterface;
}
