export interface SessionInterface {
  id?: string;

  sessionDate: Date;

  keywords: string;
  category: string;

  createdAt: Date;
  updatedAt: Date;
}
