export enum PostType {
  POST = 'post',
  EVENT = 'event'
}

export interface PostInterface {
  id?: number;
  slug: string;
  title: string;
  description: string;

  when?: Date;
  where?: string;

  image_url: string;
  type: PostType;
  createdAt: Date;

}
