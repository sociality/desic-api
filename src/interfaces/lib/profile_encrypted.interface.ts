import { ProfileInterface, EncryptInterface, EncryptedUserInterface } from '@interfaces';

export interface EncryptedProfileInterface extends ProfileInterface {
  address: EncryptInterface;
  phone: EncryptInterface;
  dateOfBirth: EncryptInterface;
  maritalStatus: EncryptInterface;
  employmentStatus: EncryptInterface;

  user?: EncryptedUserInterface;
}
