export interface ExaminationInterface {
  id?: string;

  examinationDate: Date;

  category: string;
  keywords: string;

  createdAt: Date;
  updatedAt: Date;
}
