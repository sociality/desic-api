import { UserInterface, DecryptedProfileInterface } from "@interfaces";

export interface DecryptedUserInterface extends UserInterface {
  name: string;
 // profile?: DecryptedProfileInterface;
}
