export interface AddressInterface {
    street: string;
    postCode: string;
    city: string;
    country?: string;
    long?: number;
    lat?: number;
}