import { SessionInterface, DecryptedUserInterface } from "@interfaces";

export interface DecryptedSessionInterface extends SessionInterface {
    title: string;
    description: string;

    beneficiary: DecryptedUserInterface;
    benefactor: DecryptedUserInterface;
}