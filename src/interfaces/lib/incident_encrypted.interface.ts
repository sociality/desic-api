import { IncidentInterface, EncryptedUserInterface, EncryptInterface } from "@interfaces";

export interface EncryptedIncidentInterface extends IncidentInterface {
    title: EncryptInterface;
    description: EncryptInterface;

    beneficiary: EncryptedUserInterface;
    benefactor: EncryptedUserInterface;
}