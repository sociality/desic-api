import { PostInterface } from "./post.interface";

export interface EncryptedPostInterface extends PostInterface {
  contentFiles: string;
}
