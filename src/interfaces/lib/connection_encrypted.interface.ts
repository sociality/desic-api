import { ConnectionInterface, EncryptedUserInterface } from "@interfaces";

export interface EncryptedConnectionInterface extends ConnectionInterface {
    user_a: EncryptedUserInterface;
    user_b: EncryptedUserInterface;
}