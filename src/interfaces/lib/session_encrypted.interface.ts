import { SessionInterface,EncryptedUserInterface, EncryptInterface } from "@interfaces";

export interface EncryptedSessionInterface extends SessionInterface {
    title: EncryptInterface;
    description: EncryptInterface;
    
    beneficiary: EncryptedUserInterface;
    benefactor: EncryptedUserInterface;
}