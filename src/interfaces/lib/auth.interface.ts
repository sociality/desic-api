import { EncryptInterface } from '@interfaces';
import { EncryptedUserInterface } from './user_encrypted.interface';

export interface AuthInterface {
  id?: string;

  verificationToken?: string;
  verificationExpiresIn?: string;

  emailVerified: boolean;

  restorationToken?: string;
  restorationExpiresIn?: string;

  passwordVerified: boolean;

  user: EncryptedUserInterface;
  userId: string;
}
