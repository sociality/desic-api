export interface IncidentInterface {
    id?: string;

    incidentDate: Date;

    category: string;
    keywords: string;

    createdAt: Date;
    updatedAt: Date;
}
