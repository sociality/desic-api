import { Request } from 'express';
import { EncryptedUserInterface } from '@interfaces';

export interface RequestWithUser extends Request {
  user: EncryptedUserInterface;
}