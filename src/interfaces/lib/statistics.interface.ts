import { AddressInterface, SocialInterface } from "@interfaces";

export interface OrganizationInterface {
  id?: number;
  name: string;

  phone: string;
  phone_2: string;
  fax: string;

  address: AddressInterface;

  description: string;
  description_2: string;

  social?: SocialInterface;
}
