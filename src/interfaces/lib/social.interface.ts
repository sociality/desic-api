export interface SocialInterface {
  slug: string;
  value: string;
}
