import { DecryptedUserInterface } from "@interfaces";
import { ConnectionInterface } from "@interfaces";

export interface DecryptedConnectionInterface extends ConnectionInterface {
  senderId?: string;
  sender: DecryptedUserInterface;

  receiverId?: string;
  receiver: DecryptedUserInterface;
}
