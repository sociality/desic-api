import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, In, Not } from 'typeorm';

/**
 * DTOs
 */
import { ID, ProfileDto, UserRegistrationDto } from '../dtos/index';

/**
 * Entities
 */
import { UserEntity, UserProfileEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { EncryptedUserInterface, DecryptedUserInterface, EncryptedProfileInterface, DecryptedProfileInterface, AddressInterface, ProfileInterface, UserInterface } from '@interfaces';

/**
 * Middleware
 */
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Validators
 */
import validationParams from '../validation/params.validation';
import validationBody from '../validation/body.validation';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class UserController implements Controller {
  public path = '/users';
  public router = express.Router();
  private userRepository = getRepository(UserEntity);
  private profileRepositoy = getRepository(UserProfileEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/access/:access`, authMiddleware, accessMiddleware.clientsCannotAccess, this.readUsers);
    this.router.get(`${this.path}/:id`, authMiddleware, validationParams(ID), this.readUserById);
    this.router.put(`${this.path}/:id`, authMiddleware, validationParams(ID), validationBody(ProfileDto), this.updateUser, this.updateProfile);
    this.router.put(`${this.path}/:id/:status`, authMiddleware, validationParams(ID), this.changeUserStatus);
  }

  private readUsers = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const accessRevoke: string[] = response.locals.access;
    const access: number = parseInt(request.params.access);

    let error: Error, users: EncryptedUserInterface[];
    [error, users] = await to(this.userRepository.find({
      where: {
        access: access,
        id: Not(In(accessRevoke)) // Access
      }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedUserInterface[] = users.map((o: EncryptedUserInterface) => {
      return {
        ...o, name: crypto.decrypt(o.name)
      }
    });

    response.status(200).send({
      data: { users: result },
      status: true
    })
  }

  private readUserById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const userId: ID['id'] = request.params.id;

    let error: Error, profile: EncryptedProfileInterface;
    [error, profile] = await to(this.profileRepositoy.findOne({ userId: userId }, { relations: ["user"] }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedUserInterface = {
      ...profile.user,
      name: crypto.decrypt(profile.user.name),
      profile: {
        address: JSON.parse(crypto.decrypt(profile.address)) as AddressInterface,
        phone: crypto.decrypt(profile.phone),
        dateOfBirth: new Date(crypto.decrypt(profile.dateOfBirth)),
        maritalStatus: crypto.decrypt(profile.maritalStatus),
        employmentStatus: crypto.decrypt(profile.employmentStatus)
      }
    }
    response.status(200).send({
      data: { user: result },
      status: true
    })
  }

  private updateUser = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: UserRegistrationDto = request.body;

    const userId: ID['id'] = request.params.id;
    const user: EncryptedUserInterface = await this.userRepository.findOne({ id: userId });
    let error: Error, result: any;
    [error, result] = await to(this.userRepository.save({
      ...user,
      name: crypto.encrypt(data.name)
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    next();
  }

  private updateProfile = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: ProfileDto = request.body.profile;
    const userId: ID['id'] = request.params.id;

    const profile: EncryptedProfileInterface = await this.profileRepositoy.findOne({ userId: userId });
    let error: Error, result: any;
    [error, result] = await to(this.profileRepositoy.save({
      ...profile,
      address: crypto.encrypt(JSON.stringify(data.address)),
      phone: crypto.encrypt(data.phone),
      dateOfBirth: crypto.encrypt(data.dateOfBirth),
      maritalStatus: crypto.encrypt(data.maritalStatus),
      employmentStatus: crypto.encrypt(data.employmentStatus)
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `User (with ID: ${profile.id}) has been successfully updated`,
      success: true
    });
  }

  private changeUserStatus = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const userId: ID['id'] = request.params.id;
    const status: boolean = request.params.status == 'true';

    const user = await this.userRepository.findOne(userId);

    let error: Error, result: any;
    [error, result] = await to(this.userRepository.save({
      ...user,
      activated: status
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `User (with ID: ${user.id}) has been successfully set as ${status ? 'Active' : 'Non Active'}`,
      success: true
    });
  }
}
export default UserController;
