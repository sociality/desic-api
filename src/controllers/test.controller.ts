import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
// import InvitationDto from '../communityDtos/invitation.dto'
// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
// import User from '../usersInterfaces/user.interface';
// import PartnerID from '../usersDtos/partner_id.params.dto'
// import PostEvent from '../communityInterfaces/post_event.interface';

/**
 * Middleware
 */
// import validationBodyMiddleware from '../middleware/validators/body.validation';
// import validationParamsMiddleware from '../middleware/validators/params.validation';
// import authMiddleware from '../middleware/auth/auth.middleware';
// import accessMiddleware from '../middleware/auth/access.middleware';
// import OffsetHelper from '../middleware/items/offset.helper';

/**
 * Helper's Instances
 */
// const offsetParams = OffsetHelper.offsetIndex;

/**
 * Models
 */
// import userModel from '../models/user.model';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class TestController implements Controller {
  public path = '/test';
  public router = express.Router();
  // private user = userModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.read);
  }

  private read = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {

    var x = crypto.encrypt('a');
    var y = crypto.decrypt(x);

    response.status(200).send({
      data: "OK",
      status: true
    })
  }
}

export default TestController;
