import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, getConnection, Not, In } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { CommunicationDto } from '../dtos/index';

// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { OrganizationEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { OrganizationInterface } from '@interfaces';

/**
 * Middleware
 */
import validationBody from '../validation/body.validation';

import EmailService from '../utils/email.service';
const emailService = new EmailService();

class CommunicationController implements Controller {
  public path = '/communication';
  public router = express.Router();
  private organizationRepository = getRepository(OrganizationEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/`, validationBody(CommunicationDto), this.sendMessage, emailService.clientCommunication);
  }

  private sendMessage = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: CommunicationDto = request.body;

    const organization = await this.organizationRepository.findOne();
    const result: OrganizationInterface = { ...organization, social: organization ? JSON.parse(organization.social) : null };

    response.locals = {
      ...data,
      organization: result
    };

    next();
  }
}

export default CommunicationController;
