import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, getManager, In, Not } from 'typeorm';

/**
 * DTOs
 */
import { ID, ConnectionDto } from '../dtos/index';

/**
 * Entities
 */
import { ConnectionEntity, UserEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import {
  EncryptedUserInterface, DecryptedUserInterface, UserRole,
  EncryptedConnectionInterface, DecryptedConnectionInterface,
  AccessAction, AccessCategory
} from '../interfaces/index';

/**
 * Middleware
 */
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Validators
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class ConnectionController implements Controller {
  public path = '/connections';
  public router = express.Router();
  private userRepository = getRepository(UserEntity);
  private connectionRepository = getRepository(ConnectionEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/admin`, authMiddleware, this.declareAccessCategoryAndAction, accessMiddleware.clientsCannotAccess, this.readUsersByAdmin, this.readConnectionsAdmin);
    this.router.get(`${this.path}/client`, authMiddleware, this.declareAccessCategoryAndAction, accessMiddleware.adminsCannotAccess, this.readUsersByClient, this.readConnectionsClient);
    //  this.router.get(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.readConnectionById);
    // this.router.get(`${this.path}/users`, authMiddleware, this.readConnectionToUser);
    this.router.post(`${this.path}/`, authMiddleware, validationBodyMiddleware(ConnectionDto), this.checkConnection, this.createConnection);
    //  this.router.put(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), validationBodyMiddleware(IncidentDto), this.updateIncident);
    this.router.delete(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.deleteConnection);
  }

  private declareAccessCategoryAndAction = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    response.locals = {
      action: AccessAction.COMMUNICATE,
      category: AccessCategory.ALL,
    }

    next();
  }

  // decryptUser(user: EncryptedUserInterface): DecryptedUserInterface {
  //   return (user) ? {
  //     ...user,
  //     name: crypto.decrypt(user.name),
  //     profile: this.decryptProfile
  //   } : null
  // }

  // decryptProfile(profile: EncryptedProfileInterface): DecryptedProfileInterface {
  //   return (profile) ? {
  //     address: JSON.parse(crypto.decrypt(profile.address)) as AddressInterface,
  //     phone: crypto.decrypt(profile.phone),
  //     dateOfBirth: new Date(crypto.decrypt(profile.dateOfBirth)),
  //     maritalStatus: crypto.decrypt(profile.maritalStatus),
  //     employmentStatus: crypto.decrypt(profile.employmentStatus)
  //   } : null
  // }

  private readUsersByAdmin = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const user: EncryptedUserInterface = request.user;
    const accessRevoke: string[] = response.locals.access;

    let error: Error, users: EncryptedUserInterface[];
    [error, users] = await to(this.userRepository.find({
      where: [
        {
          access: In([UserRole.USER]),
          id: Not(In([...accessRevoke, user.id]))
        }, {
          access: In([UserRole.SUPERADMIN, UserRole.ADMIN]),
          id: Not(In([user.id]))
        }
      ]
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedUserInterface[] = users.map((o: EncryptedUserInterface) => {
      return {
        ...o, name: crypto.decrypt(o.name)
      }
    });

    response.locals['users'] = result;
    next();
  }

  private readConnectionsAdmin = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const user: EncryptedUserInterface = request.user;

    let error: Error, connections: EncryptedConnectionInterface[];
    [error, connections] = await to(this.connectionRepository.find({
      where: [
        { userAId: user.id },
        { userBId: user.id }
      ], relations: ["user_a", "user_b"]
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedConnectionInterface[] = connections.map((o: EncryptedConnectionInterface) => {
      return {
        ...o, ...this.transformUsersToSenderReciever(o, user)
      }
    });

    const connectionsMerged = response.locals['users'].map((a: DecryptedUserInterface) =>
      Object.assign({},
        {
          receiverId: a.id,
          receiver: a,

          senderId: user.id,
          sender: user,

          ...this.checkConnectionToMergeAdmin(result, a)
        }
      )
    );

    response.status(200).send({
      data: { connections: connectionsMerged },
      status: true
    });
  }

  private checkConnectionToMergeAdmin(connections: DecryptedConnectionInterface[], user: DecryptedUserInterface) {
    const connection = (connections).find((b: DecryptedConnectionInterface) => (b.receiverId) === (user.id));
    if (connection) return connection;
    else return { id: 0, secret: 0 };
  }

  private readUsersByClient = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const user: EncryptedUserInterface = request.user;
    const accessRevoke: string[] = response.locals.access;

    let error: Error, users: EncryptedUserInterface[];
    [error, users] = await to(this.userRepository.find({
      where: [
        {
          access: In([UserRole.SUPERADMIN, UserRole.ADMIN]),
          id: Not(In([...accessRevoke])) // Access
        }, {
          access: In([UserRole.USER]),
          id: Not(In([user.id]))
        }
      ]
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedUserInterface[] = users.map((o: EncryptedUserInterface) => {
      return {
        ...o, name: crypto.decrypt(o.name)
      }
    });

    response.locals['users'] = result;
    next();
  }

  private readConnectionsClient = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const user: EncryptedUserInterface = request.user;

    let error: Error, connections: EncryptedConnectionInterface[];
    [error, connections] = await to(this.connectionRepository.find({
      where: [
        { userAId: user.id },
        { userBId: user.id }
      ], relations: ["user_a", "user_b"]
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedConnectionInterface[] = connections.map((o: EncryptedConnectionInterface) => {
      return {
        ...o, ...this.transformUsersToSenderReciever(o, user)
      }
    });

    const connectionsMerged = response.locals['users'].map((a: DecryptedUserInterface) =>
      Object.assign({},
        {
          receiverId: a.id,
          receiver: a,

          senderId: user.id,
          sender: user,

          ...this.checkConnectionToMergeClient(result, a)
        }
      )
    );

    response.status(200).send({
      data: { connections: connectionsMerged.filter((o: DecryptedConnectionInterface) => { return o.id > -1 }) },
      status: true
    });
  }

  private checkConnectionToMergeClient(connections: DecryptedConnectionInterface[], user: DecryptedUserInterface) {
    const connection = (connections).find((b: DecryptedConnectionInterface) => (b.receiverId) === (user.id));
    if (connection) return connection;
    else if (!connection && (user.access == UserRole.ADMIN || user.access == UserRole.SUPERADMIN)) return { id: 0, secret: 0 };
    else return { id: -1, secret: 0 };
  }

  private readConnectionById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const user: EncryptedUserInterface = request.user;
    const connectionId: ID['id'] = request.params.id;

    let error: Error, connection: EncryptedConnectionInterface;
    [error, connection] = await to(this.connectionRepository.findOne(connectionId, { relations: ['user_a', 'user_b'] }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedConnectionInterface = { ...connection, ...this.transformUsersToSenderReciever(connection, user) }

    response.status(200).send({
      data: { connection: result },
      status: true
    });
  }

  readConnectionToUser = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    // const user: EncryptedUserInterface = request.user;
    // const accessRevoke: string[] = response.locals.access; // Access
    //
    // let error: Error, connections: DecryptedConnectionInterface[];
    // [error, connections] = await to(getManager().query(`
    // SELECT u.id as "receiverId", COALESCE(ac.id, 0) as id, COALESCE(ac."secret", null) as "secret",
    // ROW(u.*::user_entity) as receiver, $1 as "senderId",
    // (SELECT ROW(user_entity.*::user_entity) FROM user_entity WHERE id=$2) as sender
    // FROM user_entity u
    // LEFT JOIN connection_entity ac
    // ON ((u.id = ac."userAId") OR (u.id = ac."userBId"))
    // WHERE u.id != $3
    //   `, [user.id, user.id, user.id]).catch());
    // if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));
    //
    // response.status(200).send({
    //   data: { connections: connections.filter((o: DecryptedConnectionInterface) => { return accessRevoke.indexOf(o.receiverId) === -1 }) },
    //   status: true
    // });
  }

  private checkConnection = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: ConnectionDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const userB = await this.userRepository.findOne({ communicationId: data.connectionId });

    let error: Error, connection: EncryptedConnectionInterface;
    [error, connection] = await to(this.connectionRepository.findOne({
      where: [
        { userAId: user.id, userBId: userB.id },
        { userBId: user.id, userAId: userB.id }
      ], relations: ["user_a", "user_b"]
    }));
    if (connection && connection.id > 0) {
      const result: DecryptedConnectionInterface = { ...connection, ...this.transformUsersToSenderReciever(connection, user) }

      response.status(200).send({
        data: { connection: result },  //`A new Connection (with ID: ${result.id}) has been successfully created`,
        success: true
      });
    } else {
      next();
    }

  }

  private createConnection = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: ConnectionDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const connection = new ConnectionEntity();
    connection.user_a = user;
    connection.user_b = await this.userRepository.findOne({ communicationId: data.connectionId });
    connection.secret = this.generateSecretToken(16);

    let error: Error, result: any;
    const newConnection = this.connectionRepository.create(connection);
    [error, result] = await to(this.connectionRepository.save(newConnection));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const x: DecryptedConnectionInterface = { ...result, ...this.transformUsersToSenderReciever(result, user) }

    response.status(200).send({
      data: { connection: x },  //`A new Connection (with ID: ${result.id}) has been successfully created`,
      success: true
    });
  }

  private deleteConnection = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const connectionId: ID['id'] = request.params.id;

    let error: Error, result: any;
    [error, result] = await to(this.connectionRepository.delete(connectionId));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Connection (with ID: ${connectionId}) has been successfully deleted`,
      success: true
    });
  }

  private transformUsersToSenderReciever(connection: EncryptedConnectionInterface, user: EncryptedUserInterface) {
    const myId: string = user.id;
    if (connection.user_a.id == myId) {
      return { sender: { ...connection.user_a, name: crypto.decrypt(connection.user_a.name) }, senderId: connection.user_a.id, receiver: { ...connection.user_b, name: crypto.decrypt(connection.user_b.name) }, receiverId: connection.user_b.id, }
    } else if (connection.user_b.id == myId) {
      return { sender: { ...connection.user_b, name: crypto.decrypt(connection.user_b.name) }, senderId: connection.user_b.id, receiver: { ...connection.user_a, name: crypto.decrypt(connection.user_a.name) }, receiverId: connection.user_a.id, }
    }
  }

  private generateSecretToken(length: number): string {
    let outString: string = '';
    let inOptions: string = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789';

    for (let i = 0; i < length; i++) {
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
    }

    return outString;
  }
}

export default ConnectionController;
