import * as express from 'express';
import { getRepository } from 'typeorm';
import to from 'await-to-ts';

import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import EmailService from '../utils/email.service';
const emailService = new EmailService();

/**
 *  Data to Transfer Objects 
 */
import { UserRegistrationDto, UserAuthenticationDto, UpdatePasswordDto, ForgotPasswordDto, RestorePasswordDto } from '../dtos/index';

/**
 * Entities
 */
import { UserEntity, UserProfileEntity, UserAuthEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/** Interfaces */
import { Controller, RequestWithUser } from '../interfaces/index';
import {
  EncryptedUserInterface, DecryptedUserInterface,
  AuthInterface, TokenDataInterface
} from '../interfaces/index';

/**
 * Middleware
 */
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/** 
 * Validators 
 */
import validationBody from '../validation/body.validation';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class AuthenticationController implements Controller {
  public path = '/authentication';
  public router = express.Router();
  private userRepository = getRepository(UserEntity);
  private profileRepository = getRepository(UserProfileEntity);
  private authRepository = getRepository(UserAuthEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/sign_in`, validationBody(UserAuthenticationDto), this.sign_in);
    this.router.post(`${this.path}/request_pass`, validationBody(ForgotPasswordDto), this.forgot_password, emailService.passwordRestoration);
    this.router.post(`${this.path}/restore_pass`, validationBody(RestorePasswordDto), this.change_password);
    this.router.put(`${this.path}/update_pass`, authMiddleware, validationBody(UpdatePasswordDto), this.update_password);
    this.router.post(`${this.path}/register_user`, authMiddleware, accessMiddleware.isAdmin, validationBody(UserRegistrationDto), this.create_user, this.create_profile, this.create_auth, emailService.userRegistration);
    this.router.delete(`${this.path}/sign_out`, this.sign_out);
  }

  private sign_in = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: UserAuthenticationDto = request.body;
    const user: EncryptedUserInterface = await this.userRepository.findOne({ email: data.email });

    if ((!user) || !(await bcrypt.compare(data.password, user.password))) {
      return response.status(400).send({
        message: 'No User with these credentials',
        success: false
      });
    }

    const results: DecryptedUserInterface = { ...user, name: crypto.decrypt(user.name) };

    response.status(200).send({
      token: this.generateAccessToken(results).token,
      success: true
    });
  }

  private sign_out = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    response.status(200).send({});
  }

  private forgot_password = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: ForgotPasswordDto = request.body;

    const user: EncryptedUserInterface = await this.userRepository.findOne({ email: data.email });
    if (!user) {
      return response.status(400).send({
        message: 'No User with these credentials',
        success: false
      });
    }

    const restoration: TokenDataInterface = this.generateTempToken(32, 2);

    const auth: AuthInterface = await this.authRepository.findOne({ userId: user.id });

    auth.restorationToken = restoration.token;
    auth.restorationExpiresIn = restoration.expiresIn.toString();

    this.authRepository.save(auth);

    response.locals = {
      email: data.email,
      token: restoration.token,
      access: user.access
    }

    next();
  }

  private change_password = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: RestorePasswordDto = request.body;

    const now = new Date();
    const seconds = parseInt(now.getTime().toString());

    const auth: AuthInterface = await this.authRepository.findOne({ restorationToken: data.tokenKey });
    if ((!auth) || (parseInt(auth.restorationExpiresIn) < seconds)) {
      return response.status(400).send({
        message: 'No User with these credentials',
        success: false
      });
    }

    const user: EncryptedUserInterface = await this.userRepository.findOne({ id: auth.userId });

    user.password = await bcrypt.hash(data.password, 10);
    this.userRepository.save(user);

    auth.restorationToken = null;
    auth.restorationExpiresIn = null;
    this.authRepository.save(auth);

    response.status(200).send({
      data: {
        user: {
          id: user.id,
          email: user.email,
          access: user.access
        },
      },
      success: true
    });
  }

  private update_password = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: UpdatePasswordDto = request.body;
    const user: EncryptedUserInterface = await this.userRepository.findOne(request.user.id);

    if ((!user) || !(await bcrypt.compare(data.password, user.password))) {
      return response.status(400).send({
        message: 'No User with these credentials',
        success: false
      });
    }

    user.password = await bcrypt.hash(data.updated, 10);
    this.userRepository.save(user);

    response.status(200).send({
      data: {
        user: {
          id: user.id,
          email: user.email,
          access: user.access
        }
      },
      success: true
    });
  }

  private create_user = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: UserRegistrationDto = request.body;

    const user: EncryptedUserInterface = await this.userRepository.findOne({ email: data.email });
    if (user) {
      return response.status(400).send({
        message: 'User with these credentials already exists',
        success: false
      });
    }

    const temp = this.generateTempToken(10, 1).token;
    const hashedPassword = await bcrypt.hash(temp, 10);

    const newUser = this.userRepository.create({
      email: data.email,
      name: crypto.encrypt(data.name),
      password: hashedPassword,
      access: data.access,
      category: data.category,
      communicationId: this.generateCommunicationId(6)
    });

    let error: Error, result: any;
    [error, result] = await to(this.userRepository.save(newUser));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.locals = {
      user: newUser,
      password: temp,
      access: data.access
    }

    next();
  }

  private create_profile = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: UserRegistrationDto['profile'] = request.body.profile;

    const newUser = this.profileRepository.create({
      userId: response.locals.user.id,
      address: crypto.encrypt(JSON.stringify(data.address)),
      phone: crypto.encrypt(data.phone),
      dateOfBirth: crypto.encrypt(data.dateOfBirth),
      maritalStatus: crypto.encrypt(data.maritalStatus),
      employmentStatus: crypto.encrypt(data.employmentStatus)
    });

    let error: Error, result: any;
    [error, result] = await to(this.profileRepository.save(newUser));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    next();
  }

  private create_auth = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const newUser = this.authRepository.create({
      userId: response.locals.user.id,
      verificationToken: '',
      verificationExpiresIn: null,
      emailVerified: true,
      restorationToken: '',
      restorationExpiresIn: null,
      passwordVerified: false
    });

    let error: Error, result: any;
    [error, result] = await to(this.authRepository.save(newUser));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: 'OK',
      success: true
    });
  }

  private generateTempToken(length: number, hours: number): TokenDataInterface {
    let outString: string = '';
    let inOptions: string = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789';

    for (let i = 0; i < length; i++) {
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
    }

    let now = new Date();
    now.setHours(now.getHours() + hours);
    let seconds = now.getTime();

    return {
      token: outString,
      expiresIn: parseInt(seconds.toString())
    }
  }

  private generateAccessToken(user: DecryptedUserInterface): TokenDataInterface {
    const expiresIn = parseInt(process.env.JWT_EXPIRATION); // an hour
    const secret = `${process.env.JWT_SECRET}`;
    const dataStoredInToken = {
      _id: user.id,
      email: user.email,
      access: user.access,
      category: user.category,
      communicationId: user.communicationId
    };

    return {
      token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
      expiresIn
    };
  }

  private generateCommunicationId(length: number): string {
    let outString: string = '';
    let inOptions: string = '0123456789';

    for (let i = 0; i < length; i++) {
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
    }

    return outString;
  }
}
export default AuthenticationController;
