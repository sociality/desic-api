import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository, In, Not } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { ID, SessionDto } from '../dtos/index';

// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { SessionEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { EncryptedSessionInterface, DecryptedSessionInterface, EncryptedUserInterface, AccessAction, AccessCategory } from '../interfaces/index';

/**
 * Middleware
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class SessionController implements Controller {
  public path = '/sessions';
  public router = express.Router();
  private sessionRepositoy = getRepository(SessionEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, authMiddleware, this.declareAccessCategoryAndAction, accessMiddleware.clientsCannotAccess, this.readSessions);
    this.router.get(`${this.path}/client`, authMiddleware, this.readSessionsByClient);
    this.router.get(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.readSessionById);
    this.router.post(`${this.path}/`, authMiddleware, validationBodyMiddleware(SessionDto), this.createSession);
    this.router.put(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), validationBodyMiddleware(SessionDto), this.updateSession);
    this.router.delete(`${this.path}/:id`, authMiddleware, validationParamsMiddleware(ID), this.deleteSession);
  }

  private declareAccessCategoryAndAction = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    response.locals = {
      action: AccessAction.READ,
      category: AccessCategory.SOCIAL,
    }

    next();
  }

  private readSessions = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const accessRevoke: string[] = response.locals.access; // Access

    let error: Error, sessions: EncryptedSessionInterface[];
    [error, sessions] = await to(this.sessionRepositoy.find({
      relations: ["benefactor", "beneficiary"],
      where: {
        beneficiaryId: Not(In(accessRevoke)) // Access
      }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedSessionInterface[] = sessions.map((o: EncryptedSessionInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { sessions: result },
      status: true
    });
  }

  private readSessionById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const sessionId: ID['id'] = request.params.id;

    let error: Error, session: EncryptedSessionInterface;
    [error, session] = await to(this.sessionRepositoy.findOne({ id: sessionId }, { relations: ["benefactor", "beneficiary"] }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedSessionInterface = {
      ...session,
      title: crypto.decrypt(session.title), description: crypto.decrypt(session.description),
      benefactor: { ...session.benefactor, name: crypto.decrypt(session.benefactor.name) },
      beneficiary: { ...session.beneficiary, name: crypto.decrypt(session.beneficiary.name) }
    };

    response.status(200).send({
      data: { session: result },
      status: true
    });
  }

  private readSessionsByClient = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const userId: string = request.user.id;

    let error: Error, sessions: EncryptedSessionInterface[];
    [error, sessions] = await to(this.sessionRepositoy.find({
      relations: ["benefactor", "beneficiary"],
      where: { beneficiaryId: userId }
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedSessionInterface[] = sessions.map((o: EncryptedSessionInterface) => {
      return {
        ...o,
        title: crypto.decrypt(o.title), description: crypto.decrypt(o.description),
        benefactor: { ...o.benefactor, name: crypto.decrypt(o.benefactor.name) },
        beneficiary: { ...o.beneficiary, name: crypto.decrypt(o.beneficiary.name) }
      }
    });

    response.status(200).send({
      data: { sessions: result },
      status: true
    });
  }

  private createSession = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: SessionDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const session = new SessionEntity();
    session.title = crypto.encrypt(data.title);
    session.description = crypto.encrypt(data.description);
    session.keywords = data.keywords;
    session.category = data.category;
    session.sessionDate = data.sessionDate;
    session.beneficiaryId = data.beneficiaryId;
    session.benefactorId = user.id;

    let error: Error, result: any;
    const newSession = this.sessionRepositoy.create(session);
    [error, result] = await to(this.sessionRepositoy.save(newSession));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));


    response.status(200).send({
      message: `A new Session (with ID: ${result.id}) has been successfully created`,
      success: true
    });
  }

  private updateSession = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: SessionDto = request.body;
    const user: EncryptedUserInterface = request.user;

    const sessionId: ID['id'] = request.params.id;
    const session = await this.sessionRepositoy.findOne(sessionId);

    let error: Error, result: any;
    [error, result] = await to(this.sessionRepositoy.save({
      ...session,
      title: crypto.encrypt(data.title),
      description: crypto.encrypt(data.description),
      benefactorId: user.id
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Session (with ID: ${session.id}) has been successfully updated`,
      success: true
    });
  }

  private deleteSession = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const sessionId: ID['id'] = request.params.id;

    let error: Error, result: any;
    [error, result] = await to(this.sessionRepositoy.delete(sessionId));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: `Session (with ID: ${sessionId}) has been successfully deleted`,
      success: true
    });
  }
}

export default SessionController;
