import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getRepository } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { OrganizationDto } from '../dtos/index';
// import CommunicationDto from '../communityDtos/communication.dto'

/**
 * Entities
 */
import { OrganizationEntity } from '../entities/index';
// import { OrganizationEntity } from 'entities';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { OrganizationInterface } from '@interfaces';

/**
 * Validators
 * */
import authMiddleware from '../middleware/auth.middleware';
import validationBody from '../validation/body.validation';
import validationParams from '../validation/params.validation';
import FilesMiddleware from '../utils/files.service';

/** Middleware's Instances */
const uploadFile = FilesMiddleware.uploadFile;
const deleteFile = FilesMiddleware.deleteFile;
const existFile = FilesMiddleware.existFile;
const deleteMany = FilesMiddleware.deleteMany;

class OrganizationController implements Controller {
  public path = '/organization';
  public router = express.Router();
  private organizationRepository = getRepository(OrganizationEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.readOrganization);
    this.router.put(`${this.path}/`, authMiddleware, this.declareStaticPath, uploadFile.single('image_url'), this.formData, validationBody(OrganizationDto), this.updateOrganization);

  }

  private declareStaticPath = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    request.params['path'] = 'static';
    request.params['type'] = 'organization';
    next();
  }

  private formData = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    request.body = {
      ...request.body,
      address: { street: request.body.street, postcode: request.body.postcode, city: request.body.city },
      social: JSON.parse(request.body.social)
    };

    next();
  }

  private readOrganization = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const organization = await this.organizationRepository.findOne();
    const result: OrganizationInterface = { ...organization, social: organization ? JSON.parse(organization.social) : null };

    response.status(200).send({
      data: { organization: result },
      status: true
    })
  }

  private updateOrganization = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const data: OrganizationDto = request.body;
    const organization = await this.organizationRepository.findOne();

    if (organization && organization['image_url'] && request.file) {
      var imageFile = (organization['image_url']).split('assets/static/');
      const file = path.join(__dirname, '../assets/static/' + imageFile[1]);
      if (existFile(file)) await deleteFile(file);
    }

    let error: Error, result: any;
    [error, result] = await to(this.organizationRepository.save({
      id: organization ? organization.id : 0,
      name: data.name,
      image_url: (request.file) ? `${process.env.API_URL}/assets/static/${request.file.filename}` : organization['image_url'],
      email: data.email,
      phone: data.phone,
      phone_2: data.phone_2,
      fax: data.fax,
      address: data.address,
      description: data.description,
      description_2: data.description_2,
      social: JSON.stringify(data.social)
    }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: "Organization Info has been successfully updated",
      success: true
    });

    // response.status(200).send({
    //   data: result,
    //   status: true
    // })
  }
}

export default OrganizationController;
