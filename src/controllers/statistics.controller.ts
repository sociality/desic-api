import * as express from 'express';
import to from 'await-to-ts';
import path from 'path';
import { getConnection, getManager, getRepository, In } from 'typeorm';

/**
 * Email Service
 */
// import EmailService from '../utils/emailService';
// const emailService = new EmailService();

/**
 * DTOs
 */
import { ID, AccessHistoryDto } from '../dtos/index';

/**
 * Entities
 */
import { UserEntity, SessionEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 */
import { Controller, RequestWithUser } from '../interfaces/index';
import { StatisticsInterface } from '../interfaces/index';

/**
 * Middleware
 */
import validationBodyMiddleware from '../validation/body.validation';
import validationParamsMiddleware from '../validation/params.validation';
import authMiddleware from '../middleware/auth.middleware';
import accessMiddleware from '../middleware/access.middleware';

/**
 * Utils
 */
import crypto from '../utils/crypto.service';

class StatisticsController implements Controller {
  public path = '/statistics';
  public router = express.Router();
  private userRepositoy = getRepository(UserEntity);
  private sessionsRepositoy = getRepository(SessionEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, /*authMiddleware,*/ this.readSessionsStatistics, this.readIncidentsStatistics, this.readExaminationsStatistics, this.readTotalStatistics);
  }

  private getPreviousMonths() {
    var dates = [];
    var i;
    for (i = 11; i > -1; i--) {
      var currentDate = new Date();;
      currentDate.setMonth(currentDate.getMonth() - i);
      dates.push(currentDate.getFullYear().toString() + "-" + ("00" + (currentDate.getMonth() + 1).toString()).slice(-2))
    }
    return dates;
  }

  private readSessionsStatistics = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    let error: Error, result: StatisticsInterface[];
    [error, result] = await to(getManager().query(`
      SELECT to_char(u."sessionDate",'YYYY-mm') as "month", COUNT(u.*) as "sessions"
      FROM session_entity u
      WHERE u."sessionDate" > date_trunc('month', CURRENT_DATE) - INTERVAL '1 year'
      GROUP BY "month";
        `).catch());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.locals['sessions'] = result;
    next();
    // const datesMerge = this.getPreviousMonths().map((a: string) =>
    //   Object.assign({},
    //     {
    //       ...this.checkSessionToMerge(result, a)
    //     }
    //   )
    // );
    // console.log(datesMerge);
    // response.status(200).send({
    //   data: { statistics: result },
    //   status: true
    // });
  }

  private readIncidentsStatistics = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    let error: Error, result: StatisticsInterface[];
    [error, result] = await to(getManager().query(`
      SELECT to_char(u."incidentDate",'YYYY-mm') as "month", COUNT(u.*) as "incidents"
      FROM incident_entity u
      WHERE u."incidentDate" > date_trunc('month', CURRENT_DATE) - INTERVAL '1 year'
      GROUP BY "month";
        `).catch());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.locals['incidents'] = result;
    next();
    // const datesMerge = this.getPreviousMonths().map((a: string) =>
    //   Object.assign({},
    //     {
    //       ...this.checkSessionToMerge(result, a)
    //     }
    //   )
    // );
    // console.log(datesMerge);
    // response.status(200).send({
    //   data: { statistics: result },
    //   status: true
    // });
  }

  private readExaminationsStatistics = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    let error: Error, result: StatisticsInterface[];
    [error, result] = await to(getManager().query(`
      SELECT to_char(u."examinationDate",'YYYY-mm') as "month", COUNT(u.*) as "examinations"
      FROM examination_entity u
      WHERE u."examinationDate" > date_trunc('month', CURRENT_DATE) - INTERVAL '1 year'
      GROUP BY "month";
        `).catch());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.locals['examinations'] = result;
    next();

    // const datesMerge = this.getPreviousMonths().map((a: string) =>
    //   Object.assign({},
    //     {
    //       ...this.checkSessionToMerge(result, a)
    //     }
    //   )
    // );
    // console.log(datesMerge);
    // response.status(200).send({
    //   data: { statistics: result },
    //   status: true
    // });
  }

  private readTotalStatistics = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const datesMerge = this.getPreviousMonths().map((a: string) =>
      Object.assign({},
        {
          ...this.checkStatisticsToMerge(response.locals.sessions, response.locals.incidents, response.locals.examinations, a)
        }
      )
    );
    response.status(200).send({
      data: { statistics: datesMerge },
      status: true
    });
  }
  private checkStatisticsToMerge(sessions: StatisticsInterface[], incidents: StatisticsInterface[], examinations: StatisticsInterface[], date: string) {
    const session = (sessions).find((b: any) => (b.month) === (date));
    const incident = (incidents).find((b: any) => (b.month) === (date));
    const examination = (examinations).find((b: any) => (b.month) === (date));

    return { month: date, sessions: session ? session.sessions : 0, incidents: incident ? incident.incidents : 0, examinations: examination ? examination.examinations : 0 }
  }
}
export default StatisticsController;
