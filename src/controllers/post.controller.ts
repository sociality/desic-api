import * as express from 'express';
import to from 'await-to-ts';
import { getRepository } from 'typeorm';
import path from 'path';

/**
 * DTOs
 * */
import { ID, PostDto } from '../dtos/index';

/**
 * Entities
 * */
import { PostEntity } from '../entities/index';

/**
 * Exceptions
 */
import UnprocessableEntityException from '../exceptions/UnprocessableEntity.exception';

/**
 * Interfaces
 * */
import { Controller, RequestWithUser } from '../interfaces/index';
import { DecryptedPostInterface, EncryptedPostInterface, PostType } from '@interfaces';

/**
 * Validators
 * */
import authMiddleware from '../middleware/auth.middleware';
import validationBody from '../validation/body.validation';
import validationParams from '../validation/params.validation';
import FilesMiddleware from '../utils/files.service';

/** Middleware's Instances */
const uploadFile = FilesMiddleware.uploadFile;
const deleteFile = FilesMiddleware.deleteFile;
const existFile = FilesMiddleware.existFile;
const deleteMany = FilesMiddleware.deleteMany;

class PostController implements Controller {
  public path = '/posts';
  public router = express.Router();
  private postRepository = getRepository(PostEntity);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    // this.router.get(`${this.path}/`, this.getAllPosts);
    this.router.get(`${this.path}/`, this.readPosts);
    this.router.get(`${this.path}/:id`, this.readPostById);
    this.router.post(`${this.path}/`, authMiddleware, this.declareStaticPath, uploadFile.single('image_url'), this.formData, validationBody(PostDto), this.createPost);
    this.router.put(`${this.path}/:id`, authMiddleware, this.declareStaticPath, validationParams(ID), uploadFile.single('image_url'), this.formData, validationBody(PostDto), this.updatePost);
    this.router.delete(`${this.path}/:id`, authMiddleware, validationParams(ID), this.deletePost);
    this.router.post(`${this.path}/image`, authMiddleware, this.declareContentPath, uploadFile.array('content_image', 8), this.uploadContentImage);

    // this.router.get(`${this.path}/init/init`, this.initializeGeneralPosts, this.initializeEducationalPosts);
  }

  private uploadContentImage = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    response.status(200).send({
      data: {
        files: request.files,
        path: `${process.env.API_URL}/assets/content/`
      },
      success: true
    });
  }

  private declareStaticPath = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    request.params['path'] = 'static';
    request.params['type'] = 'post';
    next();
  }

  private declareContentPath = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {

    request.params['path'] = 'content';
    request.params['type'] = 'post';
    next();
  }

  private formData = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    request.body = {
      ...request.body,
      when: (request.body.when != 'null') ? (request.body.when) : null,
      contentFiles: (request.body.contentFiles).split(',')
    };

    next();
  }

  private readPosts = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    let error: Error, posts: EncryptedPostInterface[];
    [error, posts] = await to(this.postRepository.find());
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedPostInterface[] = posts.map((o: EncryptedPostInterface) => {
      return {
        ...o,
        contentFiles: (o.contentFiles) ? (o.contentFiles).split(',') : []
      }
    });

    response.status(200).send({
      data: { posts: result },
      status: true
    });
  }

  private readPostById = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const postId: ID['id'] = request.params.id;

    let error: Error, post: EncryptedPostInterface;
    [error, post] = await to(this.postRepository.findOne({ id: parseInt(postId) }));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    const result: DecryptedPostInterface = {
      ...post,
      contentFiles: (post.contentFiles) ? (post.contentFiles).split(',') : []
    };

    response.status(200).send({
      data: { post: result },
      status: true
    });
  }

  private createPost = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: PostDto = request.body;

    const post = new PostEntity();
    post.slug = (data.title).replace(/[`~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '_').toLowerCase();
    post.title = data.title;
    post.description = data.description;
    post.image_url = (request.file) ? `${process.env.API_URL}/assets/static/${request.file.filename}` : '';
    post.when = data.when;
    post.where = data.where;
    post.type = data.type as PostType;
    post.contentFiles = (data.contentFiles).join();

    let error: Error, result: any;
    const newPosts = this.postRepository.create(post);
    [error, result] = await to(this.postRepository.save(newPosts));
    if (error) return next(new UnprocessableEntityException(`DB ERROR || ${error}`));

    response.status(200).send({
      message: "A new General Post (with ID: " + result.id + ") has been successfully created",
      success: true
    });
  }


  private updatePost = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const data: PostDto = request.body;

    const postID: ID['id'] = request.params.id;
    const post = await this.postRepository.findOne(parseInt(postID));

    if (post['image_url'] && request.file) {
      var imageFile = (post['image_url']).split('assets/static/');
      const file = path.join(__dirname, '../assets/static/' + imageFile[1]);
      if (existFile(file)) await deleteFile(file);
    }

    if (post.contentFiles) {
      var toDelete: string[] = [];
      (post.contentFiles).split(',').forEach((element: string) => {
        if ((data.contentFiles).indexOf(element) < 0) {
          var imageFile = (element).split('assets/content/');
          const file = path.join(__dirname, '../assets/content/' + imageFile[1]);
          toDelete.push(file);
        }
      });
      toDelete.forEach(path => existFile(path) && deleteMany(path))
    }

    let error: Error, result: any;
    [error, result] = await to(this.postRepository.save({
      id: post.id,
      slug: (data.title).replace(/[`~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '_').toLowerCase(),
      title: data.title,
      description: data.description,
      image_url: (request.file) ? `${process.env.API_URL}/assets/static/${request.file.filename}` : post['image_url'],
      when: data.when,
      where: data.where,
      type: data.type as PostType,
      contentFiles: (data.contentFiles).join()
    }));
    if (error) {
      return response.status(422).send({
        message: `DB ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "General Post (with ID: " + postID + ") has been successfully updated",
      success: true
    });
  }


  private deletePost = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const postID: ID['id'] = request.params.id;

    const post = await this.postRepository.findOne(parseInt(postID));
    if (post['image_url']) {
      var imageFile = (post['image_url']).split('assets/static/');
      const file = path.join(__dirname, '../assets/static/' + imageFile[1]);
      if (existFile(file)) await deleteFile(file);
    }

    if (post.contentFiles) {
      var toDelete: string[] = [];
      (post.contentFiles).split(',').forEach((element: string) => {
        var imageFile = (element).split('assets/content/');
        const file = path.join(__dirname, '../assets/content/' + imageFile[1]);
        toDelete.push(file);
      });
      toDelete.forEach(path => existFile(path) && deleteMany(path));
    }

    let error: Error, result: any;
    [error, result] = await to(this.postRepository.delete(parseInt(postID)));
    if (error) {
      return response.status(422).send({
        message: `DB ERROR || ${error}`,
        success: false
      });
    }

    response.status(200).send({
      message: "General Post (with ID: " + postID + ") has been successfully deleted",
      success: true
    });
  }



  // private initializeGeneralPosts = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
  //   const postsData = [
  //     {
  //       slug: 'post_a',
  //       title: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Post A'
  //         },
  //         {
  //           lang: 'el', content: 'Ανάρτηση Α'
  //         }]),
  //       description: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Main Body for post_a in english'
  //         },
  //         {
  //           lang: 'el', content: 'Περιεχόμενο για post_a στα ελληνικά'
  //         }]),
  //       type: 'post'
  //     }, {
  //       slug: 'event_b',
  //       title: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Event B'
  //         },
  //         {
  //           lang: 'el', content: 'Εκδήλωση Β'
  //         }]),
  //       description: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Main Body for event_b in english'
  //         },
  //         {
  //           lang: 'el', content: 'Περιεχόμενο για event_b στα ελληνικά'
  //         }]),
  //       type: 'event',
  //       when: new Date("2020-10-01"),
  //       where: "Melissa Address"
  //     }
  //   ];
  //
  //   const newPosts = this.generalPostRepository.create(postsData);
  //   await this.generalPostRepository.save(newPosts);
  //   next();//response.send(newPosts);
  // }
  // private initializeEducationalPosts = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
  //   const postsData = [
  //     {
  //       slug: 'edu_post_a',
  //       title: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Eduacationa Post A'
  //         },
  //         {
  //           lang: 'el', content: 'Εκπαιδευτικό Ποστ Α'
  //         }]),
  //       description: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Main Body for Eduacationa Post A in english'
  //         },
  //         {
  //           lang: 'el', content: 'Περιεχόμενο για Eduacationa Post A στα ελληνικά'
  //         }]),
  //       course: await this.courseRepository.findOne(18)
  //     }, {
  //       slug: 'edu_post_b',
  //       title: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Eduacationa Post B'
  //         },
  //         {
  //           lang: 'el', content: 'Εκπαιδευτικό Ποστ Β'
  //         }]),
  //       description: JSON.stringify([
  //         {
  //           lang: 'en', content: 'Main Body for Eduacationa Post B in english'
  //         },
  //         {
  //           lang: 'el', content: 'Περιεχόμενο για Eduacationa Post B στα ελληνικά'
  //         }]),
  //       course: await this.courseRepository.findOne(19)
  //     }
  //   ];
  //
  //   const newPosts = this.educationalPostRepository.create(postsData);
  //   await this.educationalPostRepository.save(newPosts);
  //   response.send(newPosts);
  // }
}
export default PostController;
